#include <stdio.h>
#include <string.h>
#include <math.h>
#include<immintrin.h>

int main(int argc, char **argv)
{
    const int n = 433;
    double clm;
    double A[n*n];
    double B[n*n];
    double C[n*n];
    double C1[n*n];
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            C[i+j*n]=0;
            A[i+j*n]=i+j+1;
            B[i+j*n]=i+j;
        }
    }

//Square-Dgemm Start
    const int blocksize=16;
    const int iter = n/blocksize;
    const int rest = n%blocksize;
    const int rand=n-rest;
    int l,p,mn,index_C0,index_A,index_C1,jn,kn,index_C2,index_C3;
    __m256d A_;
    __m256d B_;
    __m256d C_;
    __m256d C1_;
    __m256d C2_;
    __m256d C3_;


    for(int j=0;j<iter;j++){
        for(int k=0;k<iter;k++){
           for(int i=0;i<iter;i++){
               l=i*blocksize;
               for(int m=j*blocksize;m<j*blocksize+blocksize;m++){
                            p=k*blocksize;
                            mn=m*n;
                            index_C0 = l+m*n;
                            index_C1 = index_C0+4;
                            index_C2 = index_C0+8;
                            index_C3 = index_C0+12;
                            index_A = l+p*n;
                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[p+mn]);
                            C_ = _mm256_loadu_pd(&C[index_C0]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_loadu_pd(&C[index_C1]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_loadu_pd(&C[index_C2]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_loadu_pd(&C[index_C3]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;                            

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);

                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);                                             
                            
                            index_A+=n;     

                            A_ = _mm256_loadu_pd(&A[index_A]);
                            B_ = _mm256_set1_pd(B[++p+mn]);
                            C_ = _mm256_fmadd_pd(A_,B_,C_);
                            _mm256_storeu_pd(&C[index_C0],C_);

                            A_ = _mm256_loadu_pd(&A[index_A+4]);
                            C1_ = _mm256_fmadd_pd(A_,B_,C1_); 
                            _mm256_storeu_pd(&C[index_C1],C1_);

                            A_ = _mm256_loadu_pd(&A[index_A+8]);
                            C2_ = _mm256_fmadd_pd(A_,B_,C2_);
                            _mm256_storeu_pd(&C[index_C2],C2_);

                            A_ = _mm256_loadu_pd(&A[index_A+12]);
                            C3_ = _mm256_fmadd_pd(A_,B_,C3_);           
                            _mm256_storeu_pd(&C[index_C3],C3_);                                                                                                                                                                                                                                       

               }
           }
        }
    }

    jn=0;
    for(int j=0;j<rand;j++){
        for(int k=iter*blocksize;k<n;k++){     
            kn=k*n;   
            B_ = _mm256_set1_pd(B[k+jn]); 
            for(int i=0;i<rand;i+=4){
                    A_ = _mm256_loadu_pd(&A[i+kn]);
                    C_ = _mm256_loadu_pd(&C[i+jn]);
                    C_ = _mm256_fmadd_pd(A_,B_,C_);
                    _mm256_storeu_pd(&C[i+jn],C_);
            }
        }
        jn+=n;
    }

    for(int j=iter*blocksize;j<n;j++){
        jn=j*n;
        kn=0;
        for(int k=0;k<n;k++){
            B_ = _mm256_set1_pd(B[k+j*n]);
            for(int i=0;i<rand;i+=4){
                    A_ = _mm256_loadu_pd(&A[i+kn]);
                    C_ = _mm256_loadu_pd(&C[i+jn]);
                    C_ = _mm256_fmadd_pd(A_,B_,C_);
                    _mm256_storeu_pd(&C[i+jn],C_);
            }
            kn+=n;
        }
    }

    for(int j=iter*blocksize;j<n;j++){
        jn=j*n;
        for(int k=0;k<n;k++){
            kn=k*n;
            for(int i=rand;i<n;i++){
                C[i+jn] +=  A[i+kn]*B[k+jn];
            }
        }
    }



    jn=0;
    for(int j=0;j<rand;j++){
        kn=0;
        for(int k=0;k<n;k++){   
            for(int i=iter*blocksize;i<n;i++){
                C[i+jn] +=  A[i+kn]*B[k+jn];
            }
            kn+=n;
        }
        jn+=n;
    }




//Square-Dgemm End

  for (int i = 0; i < n; ++i)
    /* For each column j of B */
    for (int j = 0; j < n; ++j) 
    {
      /* Compute C(i,j) */
      double cij = C1[i+j*n];
      for( int k = 0; k < n; k++ )
	cij += A[i+k*n] * B[k+j*n];
      C1[i+j*n] = cij;
    }

    unsigned int wrong=0;
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            if(C1[i+j*n]!=C[i+j*n]){
                wrong++;
            }
            
        }

    }
    printf("%d", wrong);

}
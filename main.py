import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
from sklearn import datasets, linear_model
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error
import csv

values1 = genfromtxt('train.csv', delimiter=',',skip_header=1,usecols=(0,1,2,3,4,5,6,7,8,9,10,11,12,13))
kambda = [0.1,1,10,100,200]
kf = KFold(n_splits=10,shuffle=True,random_state=1) 
answers = []

for index in kambda:
    sum=0.
    model = linear_model.Ridge(alpha = index)
    for train, test in kf.split(values1):
        X = values1[train][:,1:14]
        y = values1[train][:,0]
        X1 = values1[test][:,1:14]
        y1 = values1[test][:,0]
        model.fit(X,y)
        our_predict = model.predict(X1)
        RMSE = mean_squared_error(y1, our_predict)**0.5
        sum+=RMSE
    sum/=10
    answers.append(sum)

print(answers)

np.savetxt("Bob_solution.csv", answers,fmt='%1.15f')  

/* 
    Please include compiler name below (you may also include any other modules you would like to be loaded)

COMPILER= gnu

    Please include All compiler flags and libraries as you want them run. You can simply copy this over from the Makefile's first few lines
 
CC = cc
OPT = -O3
CFLAGS = -Wall -std=gnu99 $(OPT)
MKLROOT = /opt/intel/composer_xe_2013.1.117/mkl
LDLIBS = -lrt -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm

*/
#include <immintrin.h>
const char *dgemm_desc = "Naive, three-loop dgemm.";

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */
void square_dgemm(int n, double *A, double *B, double *C)
{
    const int blocksize=8;
    const int iter = n/blocksize;
    const int rest = n%blocksize;
    const int rand=n-rest;
    int l,p,mn,index_C0,index_A,jn,kn;
    __m512d A_;
    __m512d B_;
    __m512d C_;


 #pragma omp parallel for private(mn,p,index_C0,index_A,l,A_,B_,C_)  
    for(int j=0;j<iter;j++){
        for(int k=0;k<iter;k++){
           for(int i=0;i<iter;i++){
               l=i*blocksize;
               for(int m=j*blocksize;m<j*blocksize+blocksize;m++){
                          
                            p=k*blocksize;
                            mn=m*n;
                            index_C0 = l+m*n;
                            index_A = l+p*n;
                            A_ = _mm512_loadu_pd(&A[index_A]);
                            B_ = _mm512_set1_pd(B[p+mn]);
                            C_ = _mm512_loadu_pd(&C[index_C0]);
                            C_ = _mm512_fmadd_pd(A_,B_,C_);                   
                            
                            index_A+=n;
                            
                            A_ = _mm512_loadu_pd(&A[index_A]);
                            B_ = _mm512_set1_pd(B[++p+mn]);
                            C_ = _mm512_fmadd_pd(A_,B_,C_);    

                            index_A+=n;
                            
                            A_ = _mm512_loadu_pd(&A[index_A]);
                            B_ = _mm512_set1_pd(B[++p+mn]);
                            C_ = _mm512_fmadd_pd(A_,B_,C_);    

                            index_A+=n;
                            
                            A_ = _mm512_loadu_pd(&A[index_A]);
                            B_ = _mm512_set1_pd(B[++p+mn]);
                            C_ = _mm512_fmadd_pd(A_,B_,C_);    

                            index_A+=n;
                            
                            A_ = _mm512_loadu_pd(&A[index_A]);
                            B_ = _mm512_set1_pd(B[++p+mn]);
                            C_ = _mm512_fmadd_pd(A_,B_,C_);    

                            index_A+=n;
                            
                            A_ = _mm512_loadu_pd(&A[index_A]);
                            B_ = _mm512_set1_pd(B[++p+mn]);
                            C_ = _mm512_fmadd_pd(A_,B_,C_);    

                            index_A+=n;
                            
                            A_ = _mm512_loadu_pd(&A[index_A]);
                            B_ = _mm512_set1_pd(B[++p+mn]);
                            C_ = _mm512_fmadd_pd(A_,B_,C_);    

                            index_A+=n;
                            
                            A_ = _mm512_loadu_pd(&A[index_A]);
                            B_ = _mm512_set1_pd(B[++p+mn]);
                            C_ = _mm512_fmadd_pd(A_,B_,C_);    
                            _mm512_storeu_pd(&C[index_C0],C_);                                                                                                                                       

               }
           }
        }
    }

   #pragma omp parallel for private(jn,kn,A_,B_,C_)
    for(int j=0;j<rand;j++){
	jn=j*n;
        for(int k=iter*blocksize;k<n;k++){     
            kn=k*n;   
            B_ = _mm512_set1_pd(B[k+jn]); 
            for(int i=0;i<rand;i+=8){
                    A_ = _mm512_loadu_pd(&A[i+kn]);
                    C_ = _mm512_loadu_pd(&C[i+jn]);
                    C_ = _mm512_fmadd_pd(A_,B_,C_);
                    _mm512_storeu_pd(&C[i+jn],C_);
            }
        }
    }
#pragma omp parallel for private(jn,kn,B_,A_,C_)
    for(int j=iter*blocksize;j<n;j++){
        jn=j*n;
        for(int k=0;k<n;k++){
	    kn=k*n;
            B_ = _mm512_set1_pd(B[k+j*n]);
            for(int i=0;i<rand;i+=8){
                    A_ = _mm512_loadu_pd(&A[i+kn]);
                    C_ = _mm512_loadu_pd(&C[i+jn]);
                    C_ = _mm512_fmadd_pd(A_,B_,C_);
                    _mm512_storeu_pd(&C[i+jn],C_);
            }
        }
    }
#pragma omp parallel for private(jn,kn)
    for(int j=iter*blocksize;j<n;j++){
        jn=j*n;
        for(int k=0;k<n;k++){
            kn=k*n;
            for(int i=rand;i<n;i++){
                C[i+jn] +=  A[i+kn]*B[k+jn];
            }
        }
    }



 
#pragma omp parallel for private(jn,kn)
    for(int j=0;j<rand;j++){
        jn=j*n;
        for(int k=0;k<n;k++){
	    kn=k*n;   
            for(int i=iter*blocksize;i<n;i++){
                C[i+jn] +=  A[i+kn]*B[k+jn];
            }   
        }   
    }
}
